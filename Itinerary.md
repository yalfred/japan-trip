---
id: io69x38t581z52i6eaugicb
title: Itinerary
desc: ''
updated: 1677966677387
created: 1677965899753
---
## DAY 1 (5/17)

HKG-KIX 19:45->  京都 Kyoto ~10PM

## DAY 2 - DAY 3 (5/18-5/19)

1. * Fushimi Inari-taisha Shrine (伏見稻荷大社)
	* Kiyomizu-dera (清水寺)
	* 祇園 Gion
	* 二年坂、三年坂

1. * Kinkakuji Temple (金閣寺)
   * Arashiyama (嵐山)

## DAY 4 (5/20)

Kyoto (京都) check out to Osaka (大阪)

* PM Osaka (大阪)
	* Dotombori District (道頓堀)
	*	Osaka Castle (大坂城)
	*	Tsūtenkaku 通天閣 area
	*	KUROMON MARKET - 黒門市場
	*	Umeda (梅田)
	*	Sennichimae Doguyasuji Shopping Street (千日前道具屋筋商店街)

## DAY 5 (5/21)
* Osaka (大阪) continue
	* Dotombori District (道頓堀)
	*	Osaka Castle (大坂城)
	*	Tsūtenkaku 通天閣 area
	*	KUROMON MARKET - 黒門市場
	*	Umeda (梅田)
	*	Sennichimae Doguyasuji Shopping Street (千日前道具屋筋商店街)

## DAY 6 (5/22)

1. Himeji 姬路城
2. KOBE 神戶 -> Osaka

## DAY 7 (5/23)

* Osaka trip nearby (近郊)

## DAY 8 (5/24)

* 12:55 KIX - HKG